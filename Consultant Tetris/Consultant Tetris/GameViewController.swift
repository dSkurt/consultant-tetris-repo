//
//  GameViewController.swift
//  Consultant Tetris
//
//  Created by Daniel Echegaray on 2019-05-06.
//  Copyright © 2019 Daniel Echegaray. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import Alamofire

class GameViewController: UIViewController, GenericGameSceneDelegate{
    
    

    var gameSceneLevels:[GenericGameScene] = [GenericGameScene]()
    override func viewDidLoad() {
        super.viewDidLoad()
        Alamofire.request("https://httpbin.org/get")
        Alamofire.request("https://httpbin.org/get").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
            }
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
            }
        }
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            let scene3 = GameSceneLevel3.init(size: view.bounds.size)
            gameSceneLevels.append(scene3)
            let scene2 = GameSceneLevel2.init(size: view.bounds.size)
            gameSceneLevels.append(scene2)
            let scene1 = GameSceneLevel1.init(size: view.bounds.size)
            gameSceneLevels.append(scene1)
            // Present the scene
            let scene = gameSceneLevels.popLast()
            scene?.delegateCT = self
            scene?.scaleMode = .aspectFill // Set the scale mode to scale to fit the window
            view.presentScene(scene)
            view.ignoresSiblingOrder = true
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }
    
    func gameFinished(_ state: GenericGameScene.gameFinishState){
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            self.setupNextGameScene()
        }))
        switch state {
        case .allClientsAssigned:
            alert.title = "Good job all clients are happy!"
        case .allConsultantsAssigned:
            alert.title = "Good job all consultants assigned!"
        case .noMatchingCompetencesLeft:
            alert.title = "Not any consultant left that matches clients requirements!"
        }
        self.present(alert, animated:true, completion: nil)
        
    }
    private func setupNextGameScene(){
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            // Present the scene
            view.scene?.removeAllActions()
            view.scene?.removeAllChildren()
            view.scene?.removeFromParent()
            // Present the scene
            let scene = gameSceneLevels.popLast()
            scene?.delegateCT = self
            scene?.scaleMode = .aspectFill // Set the scale mode to scale to fit the window
            view.presentScene(scene)
            view.ignoresSiblingOrder = true
            view.showsFPS = true
            view.showsNodeCount = true
            view.ignoresSiblingOrder = true
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
