//
//  ClientSpriteNode.swift
//  Consultant Tetris
//
//  Created by Daniel Echegaray on 2019-05-09.
//  Copyright © 2019 Daniel Echegaray. All rights reserved.
//

import UIKit
import SpriteKit

class ClientSN:SKSpriteNode {
    static var clientImage = "computer_guy_left"
    static var clientImageMarked = "computer_guy_left_marked"
    var requirements = ["JAVA","C++","JavaScript"]
    var requirementsString:String{
        get{
            return self.requirements.joined(separator: "\n")
        }
    }
    var isMarked:Bool = false{
        didSet{
            if isMarked{
                self.texture = SKTexture(imageNamed: ClientSN.clientImageMarked)
            }else{
                self.texture = SKTexture(imageNamed: ClientSN.clientImage)
            }
        }
    }
}
