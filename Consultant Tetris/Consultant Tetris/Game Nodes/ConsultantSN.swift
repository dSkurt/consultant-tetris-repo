//
//  ConsultantSN.swift
//  Consultant Tetris
//
//  Created by Daniel Echegaray on 2019-05-09.
//  Copyright © 2019 Daniel Echegaray. All rights reserved.
//

import UIKit
import SpriteKit
class ConsultantSN: SKSpriteNode {
    static var consultantImage = "computer_guy_right"
    static var consultantImageMarked = "computer_guy_right_marked"
    var competencesString:String{
        get{
            return self.competences.joined(separator: "\n")
        }
    }
    var competences = ["JAVA", "Objective C", "Swift"]
    var isMarked:Bool = false{
        didSet{
            if isMarked{
                self.texture = SKTexture(imageNamed: ConsultantSN.consultantImageMarked)
            }else{
                self.texture = SKTexture(imageNamed: ConsultantSN.consultantImage)
            }
        }
    }
}
