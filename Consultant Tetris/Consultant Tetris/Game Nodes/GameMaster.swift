
class GameMaster{
    var competenceList = ["JAVA",
                          "C++",
                          "C",
                          "Objective C",
                          "Javascript",
                          "Erlang",
                          "Python",
                          "Scala",
                          "Swift",
                          "Kotlin",
                          "UX",
                          "Scrum",
                          "SQL",
                          "Tableou",
                          "Agile methods",
                          "Requirement l.",
                          "Business Int.",
                          "Sketch",
                          "Prototyping"
                          ]
    var levels_seniority = ["A", "AC", "C", "SrC", "AM", "M", "SrM","P"]
    
    enum consultantMatchingState {
        case consultantAssigned
        case noMatch
        case clientFullyMatched
    }
    
    
    func chooseRandomCompetences(_ n: Int) -> [String] { return Array(competenceList.shuffled().prefix(n)) }

    func checkMatch(consultant:ConsultantSN, client:ClientSN){
        let notFoundList = client.requirements.filter( { consultant.competences.contains($0) == false } )
        print("Not found competences")
        print(notFoundList)
    }
    func assignConsultant(consultant:ConsultantSN, client:ClientSN)-> consultantMatchingState {
        let notFoundList = client.requirements.filter( { consultant.competences.contains($0) == false } )
        if notFoundList.count == client.requirements.count{
            print("No competence found")
            return consultantMatchingState.noMatch
        }else if notFoundList.isEmpty{
            print("Perfect match every competence found")
            return consultantMatchingState.clientFullyMatched
        }
        else{
            client.requirements = notFoundList
            return consultantMatchingState.consultantAssigned
        }   
    }
}
