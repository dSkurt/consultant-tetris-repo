//
//  GenericGameScene.swift
//  Consultant Tetris
//
//  Created by Daniel Echegaray on 2019-05-13.
//  Copyright © 2019 Daniel Echegaray. All rights reserved.
//

import UIKit
import GameplayKit
protocol GenericGameSceneDelegate: class {
    func gameFinished(_ state:GenericGameScene.gameFinishState)
}

class GenericGameScene:SKScene{
    enum gameFinishState{
        case allConsultantsAssigned
        case allClientsAssigned
        case noMatchingCompetencesLeft
    }
    weak var delegateCT:GenericGameSceneDelegate?
    let es_player = SKSpriteNode(imageNamed: "computer_guy_front")
    var consultantsList:[ConsultantSN] = [ConsultantSN]()
    var clientsList:[ClientSN] = [ClientSN]()
    var markedConsultant:ConsultantSN?
    var markedClient:ClientSN?
    var assignButton:SKNode!
    var assignButtonLabel:SKLabelNode!
    var infoLabel:SKLabelNode!
    var gameMaster:GameMaster!
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            self.touchUp(atPoint: t.location(in: self))
            if assignButton.contains(t.location(in: self)) && markedConsultant != nil && markedClient != nil{
                switch gameMaster.assignConsultant(consultant: markedConsultant!, client: markedClient!){
                case .consultantAssigned:
                    markedConsultant!.removeFromParent()
                    guard let idx = consultantsList.firstIndex(of: markedConsultant!) else {return}
                    consultantsList.remove(at: idx)
                    if consultantsList.isEmpty{
                        delegateCT?.gameFinished(.allConsultantsAssigned)
                    }
                case .noMatch:
                    let competences2dArr = consultantsList.map({ $0.competences })
                    let totalCompList = competences2dArr.flatMap({$0})
                    let requirements2dArr = clientsList.map({ $0.requirements })
                    let totalReqsList = requirements2dArr.flatMap({$0})
                    let notFoundList = totalReqsList.filter({ totalCompList.contains($0) == false})
                    if notFoundList.count == totalReqsList.count{
                        delegateCT?.gameFinished(.noMatchingCompetencesLeft)
                    }
                    return
                case .clientFullyMatched:
                    markedConsultant!.removeFromParent()
                    markedClient!.removeFromParent()
                    guard let consultantIdx = consultantsList.firstIndex(of: markedConsultant!) else {return}
                    guard let clientIdx = clientsList.firstIndex(of: markedClient!) else {return}
                    consultantsList.remove(at: consultantIdx)
                    clientsList.remove(at: clientIdx)
                    if clientsList.isEmpty{
                        delegateCT?.gameFinished(.allClientsAssigned)
                    }else if consultantsList.isEmpty{
                        delegateCT?.gameFinished(.allConsultantsAssigned)
                    }
                }
            }
        }
    }
    func touchUp(atPoint pos : CGPoint) {
        miniView.removeFromSuperview()
        miniView.isHidden = true
    }
    let miniView = UITextView(frame: CGRect(x: 200, y: 200, width: 100, height: 100))
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches{
            for c in consultantsList {
                if c.contains(t.location(in: self)){
                    if markedConsultant == c{
                        c.isMarked = !c.isMarked
                        markedConsultant = nil
                    }else if markedConsultant != nil{
                        markedConsultant?.isMarked = !(markedConsultant?.isMarked)!
                        c.isMarked = !c.isMarked
                        markedConsultant = c
                    }else{
                        c.isMarked = !c.isMarked
                        markedConsultant = c
                    }
                    miniView.text = c.competencesString
                    miniView.isHidden = false
                    miniView.frame.origin = c.frame.origin
                    miniView.frame.origin.x = miniView.frame.origin.x + c.frame.width
                    miniView.frame.origin.y = miniView.frame.origin.y + c.frame.height
                    miniView.frame.origin = convertPoint(fromView: miniView.frame.origin)
                    view!.addSubview(miniView)
                }
            }
            for client in clientsList {
                if client.contains(t.location(in: self)){
                    if markedClient == client{
                        client.isMarked = !client.isMarked
                        markedClient = nil
                    }else if markedClient != nil{
                        markedClient?.isMarked = !(markedClient?.isMarked)!
                        client.isMarked = !client.isMarked
                        markedClient = client
                    }else{
                        client.isMarked = !client.isMarked
                        markedClient = client
                    }
                    miniView.text = client.requirementsString
                    miniView.isHidden = false
                    miniView.frame.origin = client.frame.origin
                    miniView.frame.origin.x = miniView.frame.origin.x - client.frame.width
                    miniView.frame.origin.y = miniView.frame.origin.y + client.frame.height
                    miniView.frame.origin = convertPoint(fromView: miniView.frame.origin)
                    view!.addSubview(miniView)
                }
            }
        }
    }
    
    func touchDown(atPoint pos : CGPoint) {
    }
    func touchMoved(toPoint pos : CGPoint) {
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    

}
