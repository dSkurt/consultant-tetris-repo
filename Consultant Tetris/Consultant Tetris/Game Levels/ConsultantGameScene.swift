//
//  ConsultantGameScene.swift
//  Consultant Tetris
//
//  Created by Daniel Echegaray on 2019-05-09.
//  Copyright © 2019 Daniel Echegaray. All rights reserved.
//

import UIKit
import GameplayKit


class ConsultantGameScene: SKScene {
    let es_player = SKSpriteNode(imageNamed: "computer_guy_front")
    let consultantAmount = 7
    var consultantsList:[ConsultantSN] = [ConsultantSN]()
    let clientAmount = 3
    var clientsList:[ClientSN] = [ClientSN]()
    var markedConsultant:ConsultantSN?
    var markedClient:ClientSN?
    var assignButton:SKNode!
    var assignButtonLabel:SKLabelNode!
    var infoLabel:SKLabelNode!
    var gameMaster:GameMaster!
    override func didMove(to view: SKView) {
        gameMaster = GameMaster()
        backgroundColor = SKColor.white
        for i in  1...consultantAmount{
            let offset:CGFloat = 1.0/(CGFloat(consultantAmount)+1.0)
            let consultant = ConsultantSN(imageNamed: ConsultantSN.consultantImage)
            consultantsList.append(consultant)
            consultant.position = CGPoint(x: size.width * 0.1, y: size.height * (offset * CGFloat(i) ))
            addChild(consultant)
        }
        for i in  1...clientAmount{
            let offset:CGFloat = 1.0/(CGFloat(clientAmount)+1.0)
            let client = ClientSN(imageNamed: ClientSN.clientImage)
            clientsList.append(client)
            client.position = CGPoint(x: size.width * 0.9, y: size.height * (offset * CGFloat(i) ))
            addChild(client)
        }
        es_player.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5)
        addChild(es_player)
        
        assignButton = SKSpriteNode(color: SKColor.black, size: CGSize(width: 150, height: 44))
        assignButton.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5 - 100)
        assignButtonLabel = SKLabelNode(fontNamed: "Chalkduster")
        assignButtonLabel.position = CGPoint(x: assignButton.frame.midX, y: assignButton.frame.midY)
        assignButtonLabel.text = "Assign consultant"
        assignButtonLabel.fontSize = 14
        assignButtonLabel.fontColor = .white
        assignButtonLabel.numberOfLines = 1
        addChild(assignButtonLabel)
        addChild(assignButton)
        
        infoLabel = SKLabelNode(fontNamed: "Chalkduster")
        infoLabel.text = "0%"
        infoLabel.fontSize = 45
        infoLabel.fontColor = .black
        infoLabel.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5 + 100)
        addChild(infoLabel)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            self.touchUp(atPoint: t.location(in: self))
            if assignButton.contains(t.location(in: self)){
                if markedConsultant != nil && markedConsultant != nil{
                    gameMaster.assignConsultant(consultant: markedConsultant!, client: markedClient!)
                }
            }
        }
    }
    func touchUp(atPoint pos : CGPoint) {
        miniView.removeFromSuperview()
        miniView.isHidden = true
    }
    let miniView = UITextView(frame: CGRect(x: 200, y: 200, width: 100, height: 100))
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches{
            for c in consultantsList {
                if c.contains(t.location(in: self)){
                    if markedConsultant == c{
                        c.isMarked = !c.isMarked
                        markedConsultant = nil
                    }else if markedConsultant != nil{
                        markedConsultant?.isMarked = !(markedConsultant?.isMarked)!
                        c.isMarked = !c.isMarked
                        markedConsultant = c
                    }else{
                        c.isMarked = !c.isMarked
                        markedConsultant = c
                    }
//                    markedConsultant = markedConsultant == c ? nil : c
                    miniView.text = c.competencesString
                    miniView.isHidden = false
                    miniView.frame.origin = c.frame.origin
                    miniView.frame.origin.x = miniView.frame.origin.x + c.frame.width
                    miniView.frame.origin.y = miniView.frame.origin.y + c.frame.height
                    miniView.frame.origin = convertPoint(fromView: miniView.frame.origin)
                    view!.addSubview(miniView)
                }
            }
            for client in clientsList {
                if client.contains(t.location(in: self)){
                    if markedClient == client{
                        client.isMarked = !client.isMarked
                        markedClient = nil
                    }else if markedClient != nil{
                        markedClient?.isMarked = !(markedClient?.isMarked)!
                        client.isMarked = !client.isMarked
                        markedClient = client
                    }else{
                        client.isMarked = !client.isMarked
                        markedClient = client
                    }
                    miniView.text = client.requirementsString
                    miniView.isHidden = false
                    miniView.frame.origin = client.frame.origin
                    miniView.frame.origin.x = miniView.frame.origin.x - client.frame.width
                    miniView.frame.origin.y = miniView.frame.origin.y + client.frame.height
                    miniView.frame.origin = convertPoint(fromView: miniView.frame.origin)
                    view!.addSubview(miniView)
                }
            }
        }
    }
    
    func touchDown(atPoint pos : CGPoint) {
    }
    func touchMoved(toPoint pos : CGPoint) {
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }

}
