//
//  GameSceneLevel1.swift
//  Consultant Tetris
//
//  Created by Daniel Echegaray on 2019-05-13.
//  Copyright © 2019 Daniel Echegaray. All rights reserved.
//

import UIKit
import GameplayKit

class GameSceneLevel1: GenericGameScene {
    
    let consultantCompetenceIndexes = [[0],[1],[0]]  // Indexes that will map to the competence array
    let clientCompetenceIndexes = [[0],[0,1]]  // Indexes that will map to the competence array
    
    override func didMove(to view: SKView) {
        gameMaster = GameMaster()
        backgroundColor = SKColor.white
        for i in  1...consultantCompetenceIndexes.count{
            let offset:CGFloat = 1.0/(CGFloat(consultantCompetenceIndexes.count)+1.0)
            let consultant = ConsultantSN(imageNamed: ConsultantSN.consultantImage)
            consultant.position = CGPoint(x: size.width * 0.1, y: size.height * (offset * CGFloat(i) ))
            consultantsList.append(consultant)
            consultant.competences = consultantCompetenceIndexes[i-1].map{ gameMaster.competenceList[$0] }
            addChild(consultant)
        }
        
        for i in  1...clientCompetenceIndexes.count{
            let offset:CGFloat = 1.0/(CGFloat(clientCompetenceIndexes.count)+1.0)
            let client = ClientSN(imageNamed: ClientSN.clientImage)
            clientsList.append(client)
            client.position = CGPoint(x: size.width * 0.9, y: size.height * (offset * CGFloat(i) ))
            client.requirements = clientCompetenceIndexes[i-1].map{ gameMaster.competenceList[$0]}
            addChild(client)
        }
        es_player.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5)
        addChild(es_player)
        
        assignButton = SKSpriteNode(color: SKColor.black, size: CGSize(width: 150, height: 44))
        assignButton.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5 - 100)
        assignButtonLabel = SKLabelNode(fontNamed: "Chalkduster")
        assignButtonLabel.position = CGPoint(x: assignButton.frame.midX, y: assignButton.frame.midY)
        assignButtonLabel.text = "Assign consultant"
        assignButtonLabel.fontSize = 14
        assignButtonLabel.fontColor = .white
        assignButtonLabel.numberOfLines = 1
        addChild(assignButtonLabel)
        addChild(assignButton)
        
        infoLabel = SKLabelNode(fontNamed: "Chalkduster")
        infoLabel.text = "0%"
        infoLabel.fontSize = 45
        infoLabel.fontColor = .black
        infoLabel.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5 + 100)
        addChild(infoLabel)
    }
}
