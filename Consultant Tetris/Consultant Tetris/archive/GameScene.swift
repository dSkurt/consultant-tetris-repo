//
//  GameScene.swift
//  Consultant Tetris
//
//  Created by Daniel Echegaray on 2019-05-06.
//  Copyright © 2019 Daniel Echegaray. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let es_player = SKSpriteNode(imageNamed: "computer_guy_front")
    let consultantAmount = 4
    let clientAmount = 3
    let consultant1 = ConsultantSN(imageNamed: ConsultantSN.consultantImage)
    let consultant2 = ConsultantSN(imageNamed: ConsultantSN.consultantImage)
    let consultant3 = ConsultantSN(imageNamed: ConsultantSN.consultantImage)
    let client1 = SKSpriteNode(imageNamed: ClientSN.clientImage)
    let client2 = SKSpriteNode(imageNamed: ClientSN.clientImage)
    let client3 = SKSpriteNode(imageNamed: ClientSN.clientImage)
    let nodeTextView = UITextView(frame: CGRect(x: 200, y: 200, width: 100, height: 100))
//    var assignButton:UIButton?
    var assignButton:SKNode!
    var assignButtonLabel:SKLabelNode!
    var infoLabel:SKLabelNode!
    override func didMove(to view: SKView) {
        backgroundColor = SKColor.white
        consultant1.position = CGPoint(x: size.width * 0.1, y: size.height * 0.2)
        addChild(consultant1)
        consultant2.position = CGPoint(x: size.width * 0.1, y: size.height * 0.5)
        addChild(consultant2)
        consultant3.position = CGPoint(x: size.width * 0.1, y: size.height * 0.8)
        addChild(consultant3)
        
        client1.position = CGPoint(x: size.width * 0.9, y: size.height * 0.3)
        addChild(client1)
        client2.position = CGPoint(x: size.width * 0.9, y: size.height * 0.5)
        addChild(client2)
        client3.position = CGPoint(x: size.width * 0.9, y: size.height * 0.7)
        addChild(client3)
        es_player.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5)
        addChild(es_player)

        assignButton = SKSpriteNode(color: SKColor.black, size: CGSize(width: 150, height: 44))
        assignButton.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5 - 100)
        
        assignButtonLabel = SKLabelNode(fontNamed: "Chalkduster")
        assignButtonLabel.position = CGPoint(x: assignButton.frame.midX, y: assignButton.frame.midY)
        assignButtonLabel.text = "Assign consultant"
        assignButtonLabel.fontSize = 14
        assignButtonLabel.fontColor = .white
        assignButtonLabel.numberOfLines = 1
        addChild(assignButtonLabel)
        addChild(assignButton)
        
        infoLabel = SKLabelNode(fontNamed: "Chalkduster")
        infoLabel.text = "0%"
        infoLabel.fontSize = 45
        infoLabel.fontColor = .black
        infoLabel.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5 + 100)
        addChild(infoLabel)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            self.touchUp(atPoint: t.location(in: self))
            if assignButton.contains(t.location(in: self)){
                print("Button tapped. Assign stuff")
            }
            if consultant1.contains(t.location(in: self)){
                print("Consultant one tapped")
            }
        }
    }
    func touchUp(atPoint pos : CGPoint) {
        nodeTextView.removeFromSuperview()
        nodeTextView.isHidden = true
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches{
            if consultant1.contains(t.location(in: self)){
                nodeTextView.text = "Java\nObjective C\nSwift\nJava\nObjective C\nSwift"
                nodeTextView.isHidden = false
                nodeTextView.frame.origin = consultant1.frame.origin
                nodeTextView.frame.origin.x = nodeTextView.frame.origin.x + consultant1.frame.width
                nodeTextView.frame.origin.y = nodeTextView.frame.origin.y + consultant1.frame.height
                nodeTextView.frame.origin = convertPoint(fromView: nodeTextView.frame.origin)
                view!.addSubview(nodeTextView)
            }
        }
    }
    
    func touchDown(atPoint pos : CGPoint) {
    }
    func touchMoved(toPoint pos : CGPoint) {
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {

    }
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
